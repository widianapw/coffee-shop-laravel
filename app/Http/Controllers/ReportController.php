<?php

namespace App\Http\Controllers;

use App\Models\DetailTransaction;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index()
    {
        $bestSeller = DetailTransaction::select('*', 'products.name', DB::raw('COALESCE(SUM(amount),0) as total'))
            ->join('products', 'products.id', '=', 'detail_transactions.product_id')
            ->orderBy('total', 'desc')
            ->get();

        $monthlyIncome = DetailTransaction::select(DB::raw('MONTHNAME(detail_transactions.created_at) as month'), DB::raw('SUM(products.price * amount) as income'))
            ->join('products', 'products.id', '=', 'detail_transactions.product_id')
            ->whereYear('detail_transactions.created_at', NOW())
            ->groupBy(DB::raw("MONTH(detail_transactions.created_at)"))
            ->get();
    }
}
