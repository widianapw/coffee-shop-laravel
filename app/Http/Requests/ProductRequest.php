<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'category_id' => 'required|numeric',
            'name' => 'required|min:4',
            'price' => 'required|numeric',
            'description' => 'required|min:8',
            'discount' => 'required|numeric'
        ];

        if (in_array($this->method(), ['POST'])) {
            $rules['image'] = [
                'required',
                'max:5000'
            ];
        }

        return $rules;
    }
}
